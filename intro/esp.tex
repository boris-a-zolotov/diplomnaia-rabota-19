\begin{abstract}
We give a complete description of all convex polyhedra whose
surface can be constructed by folding and gluing (edge-to-edge) regular pentagons.
\end{abstract}

\section{Introduction}

Given a collection of 2D polygons, a \emph{gluing} describes a closed
surface by specifying how to glue each polygon edge onto another edge
from the collection.
Alexandrov's uniqueness theorem~\cite{alex} states that any gluing
that is homeomorphic to a sphere and that does not yield 
a total facial angle more than $2\pi$ at any point, is the surface of
a unique convex 3D polyhedron\footnote{Note that the original polygonal pieces
might need to be folded to obtain this 3D surface.}.

Unfortunately, the proof of this theorem is highly non-constructive
and the only known approximation algorithm has (pseudopolynomial)
running time larger than $O(n^{1116})$ (where $n$ is the total number
of edges) \cite{kpd09-approx}, and depends on the
aspect ratio of the polyhedral metric, the Gaussian curvature at its
vertices, and the desired precision of the solution.
There is no known exact algorithm for reconstructing the 3D
polyhedron, and in fact the polyhedron's coordinates might not even have a closed
formula \cite{bannister2014galois}.

Enumerating all possible valid gluings is also not an easy task, as
the number of gluings can be exponential even for a single
polygon~\cite{DDLO02}. However one valid gluing can be found in
polynomial time using dynamic programming~\cite{DO07,lo96-dynprog}.  Complete enumerations of gluings
and the resulting polyhedra are only known for very specific cases
such as the latin cross~\cite{ddlop99} and a single regular polygon~\cite{DO07}.

This paper continues the study, initiated by the first two
authors, for the special case when the polygons to be
glued together are all identical regular $k$-gons. 
For $k>6$,  the only two possibilities are two $k$-gons glued into a doubly covered $k$-gon, or one $k$-gon folded in half (if $k$ is
even). When $k=6$, the number of hexagons that can be glued into a
convex surface is unbounded, however there are at most 10 possible graph
structures for such (non-flat) polyhedra, 6 of which have been
identified, and all gluings forming doubly-covered 2D polygons have
been characterized~\cite{kl17-hex}.

Here we study the case $k=5$, i.e., gluing regular pentagons edge to edge. This case differs substantially
from the  case of hexagons, since it is not possible to produce a
vertex of Gaussian curvature $0$ by gluing pentagons. Therefore both
the number of possible graph structures and the number of possible
  gluings is constant. 
To determine the graph structure of each gluing, we use an
implementation~\cite{sech} of the Bobenko-Izmestiev algorithm to
obtain an approximate polyhedron $P$ for the gluing, and then we use a
computer program to generate a certificate that the approximation $P$ has
the correct graph structure if it is simplicial. 
For non-simplicial polyhedra, we resort to ad-hoc proofs (which appear in
the full version).