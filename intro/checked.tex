\ \newpage

\setcounter{section}{0}
\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}

	One of the directions in computational geometry is designing algorithms that provide constructive proofs of geometric theorems that are well-known but have only non-constructive proofs. This thesis deals with such situation, in particular it concerns a constructive proof of Alexandrov Theorem.

	\ms This theorem implies that if the boundary of a convex polyhedron $P$ is cut into a finite number of flat polygons $T_1 \ldots T_n$ (it is not necessary that these polygons are faces of $P$) and it is known how exactly the boundaries of the polygons were glued to each other initially, then there is a single convex polyhedron that can be obtained by gluing $T_1 \ldots T_n$ with respect to the given rules—and it is $P$ itself.

	\ms To state this theorem formally we need the following

\begin{definition}[\cite{alex}]
\label{def:razvertka}
	A \emph{net} is a set of polygons equipped with a number of rules describing the way edges of these polygons must be glued to each other.
\end{definition}

\begin{theorem}[Alexandrov, 1950,~\cite{alex}]
\label{thm:alexandrov}
	If a net is homeomorphic to a sphere and the sum of angles at each of its vertices is at most $360^\circ$ then there is a single convex polyhedron $P$ that can be glued from this net.
\end{theorem}

	\ms However, the algorithmic question of \emph{constructing} the polyhedron $P$, its 1-dimensional skeleton and coordinates of the vertices, remains open for around 60 years.

\begin{task}
\label{task:prob1}
	Given a net, calculate the coordinates of the vertices of the convex polyhedron $P$ that corresponds to it and list the edges of $P$ and the vertices they connect.
\end{task}

	\ms It is known that Problem~\ref{task:prob1} can be reduced to a system of partial differential equations~\cite{kpd09-approx,boben}. However, this method does not produce the exact answer. Moreover, there is no known algorithm for it that works faster than pseudopolynomial in $n$, where $n$ is the number of vertices.
	
	\ms If the graph structure of $P$ is known, Problem~\ref{task:prob1} can be reduced to solving a system of equalities and inequations of degrees 2 and 3. The number of variables in the system is $3n$, where $n$ is number of vertices, and the number of equalities and inequations is $O (|V|+|E|)$. The construction of this system is shown in Demaine, O'Rourke~\cite{DO07}: the equations have form
	$$|uv| = d$$
for every pair $u$, $v$ of vertices of the same face and $d$ known from the net that $P$ corresponds to, and the inequalities have form
	$$\det \begin{pmatrix}
		\overrightarrow{a-u} &
		\overrightarrow{v-u} &
		\overrightarrow{b-u}
	\end{pmatrix} \ge 0$$
for every edge $uv$ and vertices $a$, $b$ opposite to it (they state that each edge is in the „convex position“). It is also known that sometimes there is no closed-form expression for the coordinates of vertices of polyhedron in terms of edge lengths and coordinates of vertices of $T_1 \ldots T_n$~\cite{epp-closed}.
	
	\ms To get closer to a solution of Problem~\ref{task:prob1} one can solve some of its restricted cases. One way is to consider a single polygon of a particular type, e.g. the Latin cross~\cite{ddlop99} or a regular $k$-gon~\cite{DO07}. Another way is to consider only \emph{edge-to-edge} gluings, where an edge of a polygon $T_i$ needs to be glued to an entire other edge of another polygon $T_j$ (possibly $j=i$)~\cite{DO07,lo96-dynprog}.

	\ms Recently a third way of restricting the setting was proposed~\cite{kl17-hex} that on one hand is a combination of these two, and on another hand is their generalization from one single polygon to many. We wish to glue together several copies of the same regular polygon edge-to-edge. For the case of regular $k$-gons with $k>6$, the only two possibilities are: two $k$-gons glued into a doubly covered $k$-gon, or one $k$-gon folded in half (if $k$ is even)~\cite{kl17-hex}. The first interesting case when $k=6$ was also studied in~\cite{kl17-hex}—one of the reasons that did not allow the authors to complete the characterization is that they did not have a rigorous way of justifying that a given gluing corresponds to a given graph structure.

\subsection
	[Edge-to-edge gluings when $m=5$]
	{Edge-to-edge gluings when $m=5$: the list of all possible shapes}

	Edge-to-edge gluings of a number of regular pentagons are thoroughly studied in Arseneva et al.~\cite{alz-penta}. It was proved that a convex polyhedron can be made of at most 12 congruent pentagons. All possible ways to glue edges of pentagons together were considered (their number is exponential on the number of polygons)—after it one obtained the full list of the nets consisting of regular pentagons that satisfy the conditions of Theorem~\ref{thm:alexandrov}.
	
	\ms The algorithm described in Bobenko, Izmestiev~\cite{boben} was implemented in the MSc thesis of Sechelmann~\cite{sech}, the resulting  program allows for calculating the approximate coordinates of vertices of polyhedra that are glued from a number of polygons. Approximate coordinates of the polyhedra glued from regular pentagons were obtained with the use of this program that could visualize these polyhedra and provide a better understanding of their structure.

	\ms The aim of this work is to establish the true graph structure of these polyhedra. To achieve it we did the following:

\begin{enumerate}
	\item Estimated the difference between the location of edges and vertices of {\it approximate} polyhedra obtained by the algorithm and the true ones arising from Theorem~\ref{thm:alexandrov}.
	
	\item Developed automatic procedure to check whether two vertices of a polyhedron are connected by an edge or not.
\end{enumerate}

	\ms This procedure is not sufficient to achieve the goal: when there are non-triangular faces in the polyhedron, the procedure can not \emph{prove} the absence of an edge between two vertices. Thus in this work for some polyhedra we geometrically prove that they have quadrilateral faces that appear to be either trapezoids or parallelograms.

	\ms We also show that geomertical methods can be applied to prove the \emph{existence} of edges of polyhedra—as an example we consider the polyhedron glued from six regular pentagons.
	
%	\ms The result of this work is the true graph structure
%	described for all the convex polyhedra obtained
%	by gluing regular pentagons.

\subsection
	[Edge-to-edge gluings when $m = 3, 4, 6$]
	{Edge-to-edge gluings when $m = 3, 4, 6$: isomorphism of gluings.}

	The cases of triangles, quadrilaterals and hexagons are different from the case of pentagons in that the internal angles of these polygons are multiple of $360^\circ$. It means that flat vertices (the sum of angles at which is $360^\circ$) can be obtained when gluing them, and thus there are infinitely many ways to glue a number of such polygons into a convex polyhedron. Thus it is not possible to consider \emph{all} the ways to do so.

\begin{definition}
	Let us call two polyhedra \emph{combinatorially different} if they have different graph struc-\linebreak tures—i. e. different 1-dimensional skeleta.
\end{definition}

	\ms Gluings of regular hexagons were studied in~\cite{kl17-hex}. The authors have identified the 15 possible graph structures of combinatorially different polyhedra that can be glued, 5 of which are in fact doubly-covered 2D polygons. They characterized the latter ones fully, and gave examples for 5 non-flat shapes, including all simplicial ones. It is open whether the remaining 5 shapes can be realized in $\br^3$.
	
	\ms In order to classify the polyhedra obtained by gluing several polygons let us introduce the notion of isomorphic nets.

\begin{definition}
\label{def:isomSkl}
	Two nets $(T_1 \ldots T_n, \sim_1)$, $(T'_1 \ldots T'_n, \sim_2)$
	satisfying the conditions of Theorem~\ref{thm:alexandrov} are called
	\emph{isomorphic} if convex polyhedra corresponding to them are homothetic.
\end{definition}

	\ms The aim of this work is to propose a method to check whether two nets are isomorphic if given are graph structures of the polyhedra corresponding to them—for example, as a result provided by the algorithm~\cite{boben}.

	\ms For a net it is easy to find out which of its vertices are going to be the vertices of the glued polyhedron—those are the ones sum of angles at which is strictly less than $360^\circ$. It can be done in time linear on the number of vertices.

	\ms Let two nets have the same number of vertices (if not, they can not be isomorphic). In this case the isomorphism between nets can be established by

\begin{enumerate}
	\item Calculating the length of edges of the graph structures,
	
	\item Making them „normal“—for example, making the sum of edge lengths the same for both nets,
	
	\item Checking the isomorphism of planar weighted graphs obtained that can be done in polynomial time (shown in Hopcroft, Wong~\cite{hw-planar-iso}).
\end{enumerate}

	\ms To conclude, the main part of checking the isomorphism of two nets is calculating the lengths of the shortest paths between the vertices of the nets.