\subsection
	[Shortest paths on polyhedra]
	{Shortest paths on the boundaries of polyhedra}

\newcounter{tmpite}
\setcounter{tmpite}{0}

	The problem of finding shortest paths on the boundaries of the polyhedra is widely studied. Similar to the shortest path is the notion of the \emph{geodesic path}.

\begin{definition}[\cite{discr-geodes}]
\label{def:geodesPath}
	A geodesic path is a path which is locally optimal, i. e. can not be shortened by a slight perturbation.
\end{definition}
	
	\ms There are two lemmas of great importance when searching for geodesic and shortest paths that are proved in Mitchell, Mount, Papadimitriou~\cite{discr-geodes}.

\begin{lemma}[\cite{discr-geodes}, 3.2]
\label{lm:mmp-intersec}
	Let $\pi$ be a geodesic shortest path on the surface of a polyhedron $P$. The intersection between $\pi$ and a face $f$ of $P$ is a (possibly empty) line segment.
\end{lemma}

\begin{lemma}[\cite{discr-geodes}, 3.3]
\label{lm:mmp-unfold}
	If $\pi$ is a geodesic path which crosses an edge sequence $e_1 \ldots e_m$, then the planar unfolding of $\pi$ along $e_1 \ldots e_m$ is a straight line segment.
\end{lemma}

	\ms There are three well-known algorithms for computing the shortest paths, see the table below:

\def\tlabel#1{
\begin{minipage}{0.42cm}
\begin{enumerate}
	\setcounter{enumi}{\thetmpite}
	\item \label{#1} \ 
\end{enumerate}
\end{minipage}
\stepcounter{tmpite}
}

\begin{center} \small
\begin{tabular}{|c|l|l|l|l|}
\hline
	\bf № & \bf The algorithm & \bf Time complexity & \bf Space complexity &
		\makecell[l]{\bf Time to \\ \bf resonstruct the \\ \bf shortest path \\ \bf to any point} \\
\hline \hline
	\tlabel{algo:dij} & «Continuous Dijkstra»~\cite{discr-geodes} & $O(n^2 \log n)$ & $O(n^2)$ & $O(\log n)$ \\
\hline
	\tlabel{algo:chen-han} & Chen—Han Algorithm~\cite{chen-han} & $O(n^2)$ & $O(n)$ & $O(\log n)$ \\
\hline
	\tlabel{algo:optimal} & Optimal-time algorithm~\cite{ss-optimal} & $O(n \log n)$ & $O(n \log n)$ & $O(\log n)$ \\
\hline
\end{tabular}
\end{center}

	\ms One of the goals of this work is to develop an algorithm that finds the shortest paths between points
%	not on the boundary of the given polyhedron but only
on an arbitrary net satisfying the conditions of the Alexandrov's Theorem. Algorithms~\ref{algo:dij}–\ref{algo:optimal} solve this problem with an additional restriction that $T_1 \ldots T_n$ are faces of the polyhedron $P$.

\begin{figure}
	\input{figs/kvadrat}
	\caption{Shortest path $\pi = p_1p_2$ crosses parallelogramm $T$ (colored green) seven times.}
	\label{fig:cub7intersec}
\end{figure}
	
	\ms For an arbitrary net of a polyhedron and a shortest path between two points on it Lemma~\ref{lm:mmp-unfold} obviously still holds. However, Lemma~\ref{lm:mmp-intersec} does not: the cube glued from two squares and a parallelogramm (see Figure~\ref{fig:cub7intersec}) is a counterexample. Shortest path connecting points $p_1$ and $p_2$ crosses the parallelogramm seven times. One can
%	consider different polygons and ways to glue them together and
obtain an unbounded number of intersections between the shortest path and one of the polygons.

	\ms However, previously it was assumed that the Chen—Han algorithm that is designed for nets consisting of faces of a polyhedron can also be applied to arbitrary nets without any change~\cite{joe,DO07}. Still, if the number of intersections between the shortest path and a polygon from the set $T_1 \ldots T_n$ is not bounded, one can not find out when to stop running the algorithm to be sure that after we stop itwe obtain correct answer.
	
	\ms In this work we prove for some types of polygons that the number of intersections between them and an optimal path on the boundary of $P$ is bounded from above. In particular, our results imply that the modified Chen—Han algorithm runs in still the same $O(n^2)$ time on arbitrary nets consisting of regular triangles, hexagons, or squares.

\subsection{The Chen—Han Algorithm}

	When developing an algorithm that takes the boundary of a polyhedron as an input it is usually thought that this polyhedron lies in the most general class possible:

\begin{definition}
\label{def:simpl}
	\emph{Simplicial polyhedron} in $\br^3$ is a polyhedron all whose faces are triangles.
\end{definition}

	\ms Chen—Han algorithm~\cite{chen-han} is a decently effective algorithm for computing the shortest paths from a given vertex $S$ to all other vertices of a given simplicial polyhedron. Moreover, in this work it is Chen—Han algorithm that will be modified to be applied for arbitrary nets. This is why in this section we explain how this algorithm works in detail.

	\ms This algorithm constructs the \emph{sequence tree}: each node of it, apart from the root, is a triple $n' = (e, I, Proj^I_e)$ where

\begin{enumerate}
	\item $e$ is an edge of the polyhedron,

	\item $I$ is the location of the vertex $S$ (in the corresponding flat system of coordinates) after all the edges in the ancestors of $n'$ are unfolded,
	
	\item $Proj^I_e$ is a segment on the edge $e$ consisting of endpoins of all the segments starting from $I$ and intersecting all the edges in the ancestors of $n'$.
\end{enumerate}

	\ms The number of levels in the sequence tree is at most the number of faces of the polyhedron due to Lemma~\ref{lm:mmp-intersec}. On the $i$-th step, $1 \le i \le n$, for each node $(e_k, I_k, Proj^{I_k}_{e_k})$ on the $i$-th level, the algorithm does the following:

\begin{enumerate}
	\item „Unfolds“ the edge $e_k$ of the polyhedron, i. e. makes two faces incident to $e_k$ and all the faces unfolded before complanar.

	\item \label{item:whatnodes} For each of two edges $e'_1$, $e'_2$ of the „new“ face calculates $Proj^{I_k}_{e'_j}$ in accordance with Lemma~\ref{lm:mmp-unfold}. This set consists of the endpoints of all the segments starting at $S$ and intersecting $Proj^{I_k}_{e_k}$. If this set is non-empty, the algorithm inserts node
\begin{equation}
	\ll e'_j, I_k, Proj^{I_k}_{e'_j} \rr
\end{equation}
into the tree as a child of $(e_k, I_k, Proj^{I_k}_{e_k})$.
\end{enumerate}

	\ms However, the tree as described can have exponentially many nodes. This can make the time complexity of the algorithm exponential. This is why in reality Chen—Han algorithm does not insert into the tree \emph{all} the nodes described in~(\ref{item:whatnodes}). To bound the number of nodes in the sequence tree, Chen and Han proved the following

\begin{lemma}[\cite{chen-han}, 1]
\label{lm:ch-oaos}
	Given a face $ABC$ of the polyhedron $P$ and two nodes $n_1$, $n_2$ of the sequence tree on the same edge $BC$. If each of this nodes has a non-empty child on both of edges $CA$, $AB$, then at most one of the nodes $n_1$, $n_2$ can have two children which can be used to define shortest sequences.
\end{lemma}

	\ms Lemma~\ref{lm:ch-oaos} bounds from above by $O(n)$ the number of nodes on any level of the sequence tree (including its leaves). This is because any edge of the polyhedron can have at most one node on it with two children that can define shortest paths. This allows the algorithm to run in $O(n^2)$ time.

	\ms This is how Chen—Han algorithm looks in its final form:

\input{figs/algo}