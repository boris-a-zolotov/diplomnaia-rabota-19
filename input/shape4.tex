\section{Geometric methods to determine graph structure}
\label{section:geom}

\def\magl{\measuredangle}

	The main goal of this work is to establish the graph structure of polyhedra that can be obtained as edge-to-edge gluings of regular pentagons. The procedure of Section~\ref{section:algorithmic} applied to a good enough approximation of the polyhedron helps us to confirm that a certain edge is present in it. Still, if such edge is not present, the procedure will not \emph{find} it for any approximation, but we need to \emph{prove} that the edge is not there.

	\ms In this section we:

\begin{itemize}

	\item[–] Prove that there are quadrilateral faces in polyhedrons 4-2, 4-3;

	\item[–] Give an alternative geometric proof for the existence of an edge in polyhedron 6. This proof is specific for shape 6, so our automatic procedure remains to be the best way to establish that there is an edge between two vertices of a glued polyhedron.

\end{itemize}

\subsection{Quadrilateral faces of Shape 4-2}

	\emph{Shape 4-2} (see Figures~\ref{fig:4-2symm}–\ref{fig:4-2razv}) is obtained by gluing four regular pentagons edge-to-edge. In this section we prove the following

\begin{theorem}
\label{thm:4-2symm}
	The polyhedron 4-3 has four faces each of which is a trapezoid:
		$$GHCD,\ ABHG,\ EFCB,\ ADFE.$$
	Other four faces are isosceles triangles:
		$$FDC,\ EBA,\ HBC,\ GDA.$$
\end{theorem}

\begin{proof}

\begin{figure}
	\input{figs/4-2symm}
\end{figure}

\begin{figure}[ht]
	\centering
	\includegraphics[width=7cm]{figs/s/s422}
	\caption{The net of Shape 4-2.}
	\label{fig:4-2razv}
\end{figure}

	There are two vertical planes such that Shape 4-2 is symmetric with respect to both of them.
	
	\ms One of these planes, $\zeta$, is shown in Figure~\ref{fig:4-2symm}: it passes through $GH$, a common side of two pentagons, and $M_1$, $M_2$, $M_3$, the midpoints of edges $AD$, $EF$, $BC$ respectively. The reason the shape is symmetrical is that the segment $HM_2$ cuts the orange pentagon in half, and so the segment $GM_2$ does with the yellow pentagon.

	\ms The other plane passes through $E$, $F$, and midpoints of $AB$, $GH$ and $DC$.

	\ms So, if, for example, $BF$ is an edge of the polyhedron, then the segment $EC$ must also be an edge, since these segments are symmetric with respect to $\zeta$. It implies there must be a vertex of the polyhedron in the interior of a regular pentagon where $EC$ and $BF$ intersect, which can not occur. That means we found one quadrilateral face.
	
	\ms Three other quadrilateral faces can be found the same way, which completes the proof.

\end{proof}

\subsection{Quadrilateral faces of Shape 4-3}

	\emph{Shape 4-3} (see Figures~\ref{fig:4-3parall}–\ref{fig:4-3razv}) is obtained by gluing four regular pentagons edge-to-edge. Each of vertices $C$ and $E$ is surrounded by a single pentagon folded along two of its diagonals. Our aim in this section is to prove the following.
	
\begin{theorem}
\label{thm:4-3parall}
	The polyhedron 4-3 has six faces each of which is a parallelogram:
		$$EABF,\ EADH,\ CGFB,\ CGHD,\ ABCD,\ EFGH.$$

	Thus it is a parallelepiped.
\end{theorem}

\begin{proof}

\begin{figure}
	\input{figs/4-3parall}
\end{figure}

\begin{figure}[ht]
	\centering
	\includegraphics[width=7cm]{figs/s/s432}
	\caption{The net of Shape 4-3.}
	\label{fig:4-3razv}
\end{figure}

	The pentagon $EAFHA$ is folded along its diagonals $EF$ and $EH$ and glued along its edge $EA$. The shape glued this way is symmetric with respect to the plane $EAM_1$, where $M_1$ is the midpoint of $HF$.

	\ms Let us now take another pentagon $AFBDH$ and glue one of its vertices to $A$. Place this pentagon in a way that the plane $ADB$ is parallel to the plane $EHF$. To prove that it is possible to glue the two pentagons along the edges $AF$ and $AH$ without changing the position of the triangle $ADB$ it suffices to prove that
	$$\measuredangle FEA + \measuredangle EAF + \measuredangle FAB = 180^\circ.$$

	\ms Indeed, $\measuredangle FAB = \measuredangle AFE$ is the angle between the diagonal of a pentagon and the side of it, therefore the sum above is equal to the sum of angles of the triangle $EAF$, which is clearly $180^\circ$.
	
	\ms So, $|AB| = |EF|$ and $AB \parallel EF$, which implies that $EABF$ is a parallelogram, and so are $EADH$, $CGFB$, and $CGHD$. Also, the shape obtained by gluing the pentagons $EAFHA$ and $AHDBF$ is still symmetric with respect to the plane $EAM_1$, and the planes $EHF$ and $ADB$ are parallel.

	\ms Now let us observe that $HDBF$ is a square. Indeed, all of its sides are equal as sides of a regular pentagon, and it has an axis of symmetry passing through the midpoints $M_1$ and $M_2$ of its opposite sides. Now if we glue the two halves of the shape along this common square, the triangles $CDB$ and $ADB$ will be coplanar, since
	$$\magl CM_2M_1 = \magl EM_1M_2\text{\quad and\quad}\magl
		EM_1M_2 + \magl M_1M_2A = 180^\circ.$$
	
	\ms Since $AD = DC = CB = BA$ as diagonals of a regular pentagon, $ADCB$ is a rhombus, and so is $EHGF$. Now the proof is complete.

\end{proof}