\section{Shortest paths on various tiles}
\label{section:shortestPath}

Let $P$ be a convex polyhedron, $T$ be a convex polygon drawn on the surface of $P$, and $p_1$, $p_2$ be two points on the surface of $P$. Also let $\pi$ be a geodesic shortest path connecting them.

\begin{lemma}
\label{lm:shortestPathContents}
	The intersection between $\pi$ and $T$ is a union of a finite number of segments $a_1b_1 \ldots a_mb_m$\scolon all the points $a_i$, $b_i$ lie on the boundary of $T$, except possibly $a_1$ and $b_m$.
\end{lemma}

%	\begin{proof}
%	Any connected part of $\pi$ in $T$ is a segment since it is a geodesic path. The
%	endpoints of the segments must lie on the boundary of $T$ unless these
%	endpoints are either $p_1$ or $p_2$ in order for the path to be connected.
	
%	\ms Assume that the number of segments is infinite—then there exists
%	a limit point $x$ of their endpoints. The boundary of $T$ is a closed set thus
%	$x$ belongs to it. Then $x$ lies on an edge $l$ of the boundary of $T$.
%	Thus infinitely many $a_i$-s and $b_i$-s also lie on this edge.

%	Let us consider $l$ as a subset of $\br$. Then the infinite number of $a_i$-s
%	and $b_i$-s lying on $l$ can be replaced with two points: the minimum and the
%	maximum of all of them (the set consisting of these points is closed and bounded
%	because it is the intersection between $\pi$ and $l$), since the edge is the shortest
%	path between any two points of it.
%	\end{proof}

\begin{definition}
	Let $a_ib_i$ be a segment of the intersection between $\pi$ and $T$ with $a_i, b_i \in \partial T$. The points $a_i$, $b_i$ divide the boundary of $T$ into two parts. Let there be $n_1$ and $n_2$ be the numbers of vertices of $\partial T$ in the parts respectively. Then the segment $a_ib_i$ has \emph{type $s$} if
	$$\min (n_1, n_2) = s.$$

	If $T$ is a convex $r$-gon then the possible types of a segment can range between 1 and $\lfloor r / 2 \rfloor$ (see Figure~\ref{fig:segmType}).
\end{definition}

\begin{lemma}
\label{lm:pastFuture}
	If $a_ib_i$ is a segment of the intersection between $\pi$ and $T$, then

\begin{enumerate}
	\item \label{item:past} no point of $a_1b_1, \ldots, a_{i-1}b_{i-1}$ may lie in the disk centered at $b_i$ with radius $|a_ib_i|$ (shown in Figure~\ref{fig:past}),
	\item \label{item:future} no point of $a_{i+1}b_{i+1}, \ldots, a_mb_m$ may lie in the disk centered at $a_i$ with radius $|a_ib_i|$ (shown in Figure~\ref{fig:future}),
	\item \label{item:noother} no point of $a_jb_j$, $j \ne i$ may lie inside the disk having $a_ib_i$ as its diameter.
\end{enumerate}
\end{lemma}

\begin{figure}
	\input{figs/past-and-future-circles}
\end{figure}

\begin{proof}

	Let us prove item~(\ref{item:past}). If there is a point $p$ of a segment $a_jb_j$ preceding $a_ib_i$ inside that circle then (because $T$ is convex) the segment $pb_i$ lies entirely inside $T$ and is shorter than $a_ib_i$. Then we can replace the path
	$$\pi = \ldots a_jpb_j \ldots a_ib_i \ldots$$
with
	$$\ldots a_jpb_i \ldots,$$
which is shorter than $\pi$, thus $\pi$ is not the shortest.

	\ms The proof of~(\ref{item:future}) is analogous. To prove~(\ref{item:noother}) let us note that the intersection between the disks from~(\ref{item:past}) and~(\ref{item:future}) covers the smaller circle we are considering (see Figure~\ref{fig:typeOne})—thus no segments either from „the past“ or „the future“ of path $\pi$ can lie inside it.

\end{proof}

\begin{lemma}
\label{lm:typeOne}
	If the angle at a vertex $v$ of polygon $T$ is not less than $90^\circ$ then there is at most one segment $a_ib_i$ of the intersection $\pi \cap T$ of type 1 such that the vertex $v$ lies between points $a_i$ and $b_i$.
\end{lemma}

\begin{figure}
	\input{figs/typeone}
\end{figure}

\begin{proof}

	Suppose there are multiple segments satisfying the statement of the lemma. Let us denote by $a_ib_i$ the one whose end is the farthest from $v$. Due to Lemma~\ref{lm:pastFuture} all the points of the segments $a_jb_j$, $j \ne i$ of the intersection $\pi \cap T$ must lie outside the disk $D$ whose diameter is $a_ib_i$.
	
	\ms This disk covers all the points $s$ such that $\measuredangle a_isb_i \ge 90^\circ$, thus it covers the triangle $a_ivb_i$ (see Figure~\ref{fig:typeOne}) and consequently no other points of the path $\pi$ can lie inside this triangle.

	\ms All the other segments satisfying the statement have at least some of their points lying inside $a_ivb_i$. Thus we arrive to a contradiction: $\pi$ is not the shortest path.

\end{proof}

\subsection{Shortest paths crossing squares}

\begin{lemma}
\label{lm:squareTypeTwo}
	Let $T = ABCD$ be a square, then there is at most one segment
	of type 2 in $\pi \cap T$.
\end{lemma}

\begin{proof}
	A segment of type 2 connects either $AB$ with $CD$ or $BC$ with $DA$. Since $\pi$ is a shortest path, segments that are parts of $\pi$ can not intersect each other. Therefore, without loss of generality all the segments of type 2 connect $AB$ with $CD$.
	
	\ms Among all these segments let us consider the segment $a_ib_i$ of type 2 with the greatest possible~$i$. Without loss of generality $a_i \in AB$, $b_i \in CD$. The disk centered at $b_i$ with radius $|a_ib_i| \ge |CD|$ covers $CD$—thus, by Lemma~\ref{lm:pastFuture}, there are no segments $a_jb_j$, $j<i$ with endpoints on $CD$. It means that $a_ib_i$ is the only segment of type 2, which completes the proof.
\end{proof}

\begin{theorem}
\label{thm:shortestSquare}
	If $T$ is a square and $\pi$ is a geodesic shortest path between two points $p_1$, $p_2$ then the intersection between $\pi$ and $T$ consists of at most 7 segments.
\end{theorem}

\begin{proof}

	A segment in the intersection can either have type 1, type 2 or contain $p_1$ or $p_2$.
	
\begin{enumerate}
	
	\item By Lemma~\ref{lm:typeOne}, there are at most 4 segments of type 1: at most one for each vertex\scolon
	
	\item By Lemma~\ref{lm:squareTypeTwo}, there is at most one segment of type 2\scolon
	
	\item For each of the points $p_1$, $p_2$ at most one segment contains it.

\end{enumerate}

	This sums up to $4+1+2=7$ segments.

\end{proof}

\begin{remark}
	If $p_1$ and $p_2$ lie outside $T$ or on its boundary, the number of segments in $\pi \cap T$ is at most $5$.
\end{remark}

%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%

\subsection{Shortest paths crossing regular hexagons}

\begin{lemma}
\label{lm:hexTypeTwo}
	If $T$ is a regular hexagon and $u$, $v$ is a pair of its vertices adjacent to each other, then there is at most one segment of type 2 with vertices $u$, $v$ between its endpoints.
\end{lemma}

\begin{figure}
	\input{figs/hextypetwo}
\end{figure}

\begin{proof}

	Let us consider a segment $a_ib_i$ of type 2 with vertices $u$, $v$ between its endpoints with largest possible $i$, i.~e. the last segment traversing $T$. Clearly, $|a_ib_i| \ge |uv|$ as the part of $a_ib_i$ bounded by the perpendiculars raised from $u$ and $v$ (shown in Figure~\ref{fig:hexTypeTwo}) is not shorter than $uv$.
	
	\ms Therefore the circle centered at $b_i$ of radius $|a_ib_i|$ covers the side of $T$ that contains $b_i$ (as $T$ is regular and $uv$ is longer than any of its sides). Thus, by Lemma~\ref{lm:pastFuture}, no segments $a_jb_j$ with $j < i$ can intersect with that side, which finishes the proof.

\end{proof}

\begin{theorem}
\label{thm:shortestHex}
	If $T$ is a regular hexagon and $\pi$ is a geodesic shortest path between two points $p_1$, $p_2$ then the intersection between $\pi$ and $T$ consists of at most 12 segments.
\end{theorem}

\begin{proof}

	A segment in the intersection can either have type 1, type 2, type 3 or contain $p_1$ or $p_2$.

\begin{enumerate}

	\item According to Lemma~\ref{lm:typeOne}, there are at most 6 segments of type 1: at most one for each vertex\scolon
	
	\item According to Lemma~\ref{lm:hexTypeTwo}, there are at most 3 segments of type 2: there can not be more than 3 pairs of adjacent vertices such that the corresponding segments of type 2 do not intersect\scolon
	
	\item Finally, there is at most one segment of type 3: the proof for this is analogous to one of Lemma~\ref{lm:squareTypeTwo}\scolon

	\item At most one segment can contain each of the points $p_1$, $p_2$.

\end{enumerate}

	This totals in $6+3+1+2=12$ segments.

\end{proof}

\subsection{Shortest paths crossing regular $n$–gons}

The results above can be generalized to polygons with more vertices:

\begin{theorem}
\label{thm:regNgon}
	If $T = v_1v_2 \ldots v_n$ is a regular $n$–gon with $n > 5$ and $\pi$ is a geodesic shortest path between two points $p_1$, $p_2$ then the intersection between $\pi$ and $T$ consists of at most $n+3$ segments.
\end{theorem}

\begin{proof}

	Let us first establish that there is at most one segment of „maximum“ type—$\lfloor \frac{n}{2} \rfloor$. If $n$ is even then there is only one pair of sides of $T$ that can be connected by non-intersecting segments of maximum type. If $n$ is odd, then there can be two such pairs of sides sharing a common edge, say—
	$$V_nV_1,\ \ V_{\frac{n+1}{2}-1}V_{\frac{n+1}{2}}\text{\quad
		and\quad}V_nV_1,\ \ V_{\frac{n+1}{2}}V_{\frac{n+1}{2}+1}.$$

\begin{figure}
	\input{figs/shortestNeigh}
	\caption{Shortest neighboring diagonal for segment $ab$ is $KG$.}
	\label{fig:shortestNeigh}
\end{figure}

	\ms Let us consider the segment $a_ib_i$ of maximum type with the largest $i$. Its length is bounded below by the length of the shortest among two neighboring diagonals of $T$ (see Figure~\ref{fig:shortestNeigh}), which is equal to
$$
	2 \cdot \sin \ll \frac{360^\circ}{n} \cdot
		\left\lfloor \frac{n-2}{2} \right\rfloor \cdot
		\frac{1}{2}
	\rr, 
$$
which is more than the radius needed to cover two contiguous sides of $T$, equal to
$$
	2 \cdot \sin \ll \frac{360^\circ}{n} \rr.
$$

	\ms Therefore the circle centered at $b_i$ with radius $|a_ib_i|$ covers the sides of $T$ adjacent to the one containing $b_i$ (including that side too). Applying Lemma~\ref{lm:pastFuture} we can finish the proof with $a_ib_i$ being the only segment of the maximum type.
	
	\ms Let us now bound the number of segments of lesser types. We will establish that if $a_ib_i$ is a segment of type strictly less than $\lfloor \frac{n}{2} \rfloor$, than for any vertex $v$ between its endpoints
	$$\measuredangle a_ivb_i \ge 90^\circ.$$
	
	\ms If $n$ is even then there are diagonals of $T$ which are diameters of the circumcircle of $T$. For each such diagonal $v_pv_q$ the angle $v_pvv_q$ is equal to $90^\circ$. Let us find such $v_p$, $v_q$ that $a_i$ and $b_i$ lie on the boundary of the half of $T$ defined by the vertices $v_p \ldots v \ldots v_q$.
	
\begin{figure}
	\input{figs/regngon}
\end{figure}

	\ms Since the half of a regular polygon is convex,
	$$\measuredangle a_ivb_i\ \ge\ \measuredangle v_pvv_q\ =\ 90^\circ
		\text{\quad(see Figure~\ref{fig:regNgon}).}$$

	\ms The proof for odd $n$'s is similar.

	\ms Since for any vertex $v$ between $a_i$ and $b_i$ the angle $a_ivb_i \le 90^\circ$ the circle having $a_ib_i$ as its diameters covers all such vertices. Since both the regular $n$--gon and the circle are convex, all the edges between $a_i$ and $b_i$ are also covered.
	
	\ms According to Lemma~\ref{lm:pastFuture}, for every edge of $T$ at most two segments of $\pi \cap T$ and of type less than $\lfloor \tfrac{n}{2} \rfloor$ can have their endpoints on this edge: the circle of the first one covering the left side of the edge, the circle of the second one coverting the right side of the edge. Thus there are at most $2n$ \emph{endpoints} of the segments of lesser types in $\pi \cap T$, which results in at most $n$ segments.
	
	\ms In total there are at most $n+1+2$ segments: $1$ of the maximum type, $n$ of the lesser types and $2$ possibly containing $p_1$ and $p_2$, which finishes the proof.

\end{proof}

\begin{corollary}
\label{thm:e2eHex}
	If $T=ABCDEF$ is a regular hexagon, the points $p_1$, $p_2$ lie outside its interior, and $\pi$ is the shortest path between $p_1$ and $p_2$, then the intersection $\pi \cap T$ consists of at most 6 segments.
\end{corollary}

\begin{proof}

	If there are no segments of type 3, then by Theorem~\ref{thm:regNgon} there are at most $6$ segments. If there is a segment of type 3 (there is at most one due to Theorem~\ref{thm:shortestHex}), then we have to bound the number of segments of the lesser types.

\begin{figure}
	\input{figs/e2ehex}
\end{figure}

	\ms Let the only segment of type 3 connect the sides $BC$ and $EF$. First let us note that no segments of type 2 are possible in this case: if the vertices between the endpoints of a segment of type 2 are $B$, $C$ or $E$, $F$ then this segment intersects with $a_ib_i$.

	\ms If not, then this segment must intersect $DA$. Note that
	$$|a_iA|, |b_iA|, |a_iD|, |b_iD| \le \ell \le |a_ib_i|,$$
	where $\ell$ is the length of a side of the equilateral triangle $ACE$ (see Figure~\ref{fig:e2eHex}). It means that the points $A$, $D$ belong to both circles from Lemma~\ref{lm:pastFuture}, and according to this lemma no segments of $\pi$ can intersect the convex hull $Aa_iDb_i$ (since these four points belong to both circles and the circles are convex).

	\ms Thus no segments of type 2 are possible. Also not possible are the segments of type 1 with the vertices $A$ or $D$ between their endpoints. Finally we have the upper bound of
	$$1\,\text{(of type 3)}\ +\ 4\,\text{(of type 1 corresponding to $B,C,E,F$)}\ 
		=\ 5\,\text{segments,}$$
	which finishes the proof.

\end{proof}