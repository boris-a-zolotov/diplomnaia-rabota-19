------------------------
-- Элементы в упорядоченных тройках.
-- Определители 2×2 и 3×3
------------------------

fsth (a,b,c) = a
sndh (a,b,c) = b
trdh (a,b,c) = c

deta a b c d = a*d - b*c

dett (a,b,c) (d,e,f) (g,h,k) = a * (deta e f h k) - b * (deta d f g k) + c * (deta d e g h)

------------------------
-- Применение функции к тройке аргументов, минус в пространстве R³.
-- Расстояние между точками, скалярное произведение в R³.
-- Косинус угла между векторами QP, QR.
------------------------

trifunc :: (a -> a -> a) -> (a,a,a) -> (a,a,a) -> (a,a,a)
trifunc h (x,b,c) (d,e,f) = (h x d, h b e, h c f)

triminus p q = trifunc (\x y -> x-y) p q

dotProd (a,b,c) (d,e,f) = a*d + b*e + c*f

distance (a,b,c) (d,e,f) = sqrt $ (a-d)^2 + (b-e)^2 + (c-f)^2

cosangle p q r = (dotProd (triminus p q) (triminus r q)) / ((distance q p) * (distance q r))

------------------------
-- Первые три точки—результата Align.
-- Вторая — ноль, третья — на оси y.
-- Первая — получается откладыванием угла от оси y.
------------------------

firstPoint p q r s = (-1 * d * sqrt(1-c*c),  d * c,  0)
  where
    d = distance p q
    c = cosangle p q r

secondPoint p q r s = (0, 0, 0)

thirdPoint p q r s = (0,  distance q r,  0)

------------------------
-- Проекция вектора QP на вектор QR.
-- Высота треугольника PQR, опущенная
-- из вершины P — как вектор, от основания к вершине.
------------------------

vectorProj p q r = (\a (x,y,z) -> (a*x , a*y , a*z)) ((dotProd qr qp) / (d*d)) qr
  where
    qp = triminus p q
    qr = triminus r q
    d = distance q r

vectorHeight p q r = triminus qp (vectorProj p q r)
  where
    qp = triminus p q
    qr = triminus r q

------------------------
-- Косинус угла между плоскостями PQR, SQR.
-- Длина проекции отрезка QS на отрезок QR, который впоследствии
-- станет осью y. Расстояние от точки S до отрезка QR.
------------------------

planeAngle p q r s = (dotProd h1 h2) / (d1 * d2)
  where
    h1 = vectorHeight p q r
    h2 = vectorHeight s q r
    d1 = distance (0,0,0) h1
    d2 = distance (0,0,0) h2

fourthPointProjection p q r s = distance (0,0,0) (vectorProj s q r)

fourthPointLeg p q r s = distance (0,0,0) (vectorHeight s q r)

------------------------
-- Четвёртая точка—результат Align.
-- По оси y — откладывает длину проекции на QR.
-- По осям x, z — берём высоту и поворачиваем её
-- в соответствии с углом между плоскостями PQR, SQR.
------------------------

detOrder p q r s = dett qp qr qs
  where
    qp = triminus p q
    qr = triminus r q
    qs = triminus s q

fourthPoint p q r s = (-1 * l * c , pr , sig * l * sqrt(1-c*c))
  where
    l = fourthPointLeg p q r s
    c = planeAngle p q r s
    pr = fourthPointProjection p q r s
    sig = (-1) * signum (detOrder p q r s)

------------------------
-- P, Q, R, S  UD
-- Q, R, P, S  DU
-- R, P, Q, S  DU
------------------------

------------------------
-- Коэффициенты прямой, проходящей через две точки.
-- Условие того, что точка
-- *в достаточной степени под* прямой.
------------------------

firstCoeff xa ya xb yb = (yb - ya) / (xb - xa)

secndCoeff xa ya xb yb = (xb * ya - xa * yb) / (xb - xa)

lineDistance x y k b = (k*x - y + b) / (sqrt $ k*k + 1)

wellBelow alpha x y xa ya xb yb = (y < k*x + b) && (lineDistance x y k b > alpha)
  where
    k = firstCoeff xa ya xb yb
    b = secndCoeff xa ya xb yb

------------------------
-- *Три плоскости*. Мы всегда смотрим на первую
--  и третью координаты — около оси *y*, у точки, которая
--  не там, и у точки, которую мы двигаем по пространству.
------------------------

firstPlane alpha p q r s = wellBelow alpha xs zs (xp + alpha) (zp + alpha) (-1 * alpha) (-1 * alpha)
  where
    xs = fsth $ fourthPoint p q r s
    zs = trdh $ fourthPoint p q r s
    xp = fsth $ firstPoint p q r s
    zp = trdh $ firstPoint p q r s

secndPlane alpha p q r s = wellBelow alpha xs zs (xp + alpha) (zp - alpha) (-1 * alpha) alpha
  where
    xs = fsth $ fourthPoint q r p s
    zs = trdh $ fourthPoint q r p s
    xp = fsth $ firstPoint q r p s
    zp = trdh $ firstPoint q r p s

thirdPlane alpha p q r s = wellBelow alpha xs zs (xp + alpha) (zp - alpha) (-1 * alpha) alpha
  where
    xs = fsth $ fourthPoint r p q s
    zs = trdh $ fourthPoint r p q s
    xp = fsth $ firstPoint r p q s
    zp = trdh $ firstPoint r p q s

theresAnEdge alpha p q r s = (firstPlane alpha p q r s) && (secndPlane alpha p q r s) && (thirdPlane alpha p q r s)

------------------------
-- Тест на реальной фигуре: 8.
------------------------

a = (1.7964059390811467,0.0,0.0)
b = (1.0741858668825588,0.3774601538724013,0.5795877841641639)
c = (0.4627696824462514,-0.37047892722529857,0.8379622805377634)
d = (0.4627696824462251,-0.9162070816198993,-0.000000000674378481)
e = (1.0741858668825224,-0.3774601544164369,-0.5795877838098117)
f = (0.46276968244613376,0.37047892643871677,-0.8379622808853392)
g = (0.4627696824461601,0.9162070813351211,0.0)
h = (-1.796405938942955,0.0000000000684636823,-0.00000000034584134394)
i = (-1.0741858671205824,-0.579587784025112,0.3774601534086557)
j = (-0.46276968266375634,-0.0000000009484149785,0.9162070812362096)
k = (-0.4627696822356244,0.8379622786720125,0.3704789273536469)
l = (-1.0741858665333857,0.5795877843576744,-0.3774601545686791)
m = (-0.4627696825916457,-0.8379622802654594,-0.3704789276593546)
n = (-0.462769682167164,0.0000000011376730667,-0.9162070817606903)
aalpha = 0.0001

main = do
  print $ theresAnEdge aalpha d e a f
  print $ theresAnEdge aalpha e f a g
  print $ theresAnEdge aalpha f g a b
  print $ theresAnEdge aalpha g b a c
  print $ theresAnEdge aalpha b c a d
  print $ theresAnEdge aalpha g d a e
