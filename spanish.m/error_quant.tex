\subsection{Precision of vertex location \\ based on the approximation}
\label{section:precis}

In this section our aim is to find a small real number $r$ such that each vertex of $\mathcal P$ lies within an $r$--ball centered at the corresponding vertex of $P$.

\hypertarget{wlog}{Without loss of generality,} one of the vertices of
both $P$ and $\P$ (let them be $u_1$ and $v_1$ respectively) is
located at $(0,0,0)$, one of the edges incident to this vertex (in
$\P$ and in $P$, respectively) is aligned with the $x$ axis, and one
of the faces incident to that vertex and that edge --- both in $\P$ and
$P$ --- lies on the horizontal plane $z=0$.

The main result of this section is the following

\begin{theorem}
\label{precision}
	Suppose $\mu$ is the maximum edge discrepancy between $P$ and $\P$, $\gamma$ is the maximum angle discrepancy between $P$ and $\P$, $\mathcal D$ is the maximum degree of a vertex of $P$. If $\mathcal D \gamma < \pi / 2$, then each vertex of $\P$ lies within an $r$-ball centered at the corresponding vertex of $P$, where
	$r = E^2 \cdot  L \cdot 2 \sin ( \mathcal D \gamma / 2 ) + E \mu$.
\end{theorem}

To prove this theorem we need two lemmas:

\begin{lemma}
\label{lm:singlesegm}
Let $pq$, $pq'$ be line segments in $\br^3$. If there are two real numbers $\varepsilon$, $\theta$ with $\varepsilon > 0$ and $0 < \theta < \tfrac{\pi}{2}$ such that

\begin{itemize}

	\item[{\itshape (a)}] $|pq|-\varepsilon \le |pq'| \le |pq|+\varepsilon$,
	
	\item[{\itshape (b)}] $\measuredangle qpq' \le \theta$,

\end{itemize}

then\quad $|qq'| \le 2 |pq| \sin\frac{\theta}{2} + \varepsilon.
	\quad\refstepcounter{equation}\hfill(\theequation)\label{eq:singlesegm}$
\end{lemma}

To find $r$ we carry out the following procedure: \vspace{-0.25cm}

\begin{itemize}
	
	\item Consider a vertex $v$ of $\P$ and the shortest path
          $v_1w_1w_2\ldots w_kv$ from $v_1$ to $v$ in the graph
          structure of $\P$ --- it is comprised of edges of $\P$ and is not the geodesic shortest path from $v_1$ to $v$; \vspace{-0.25cm}

	\item Using Lemma~\ref{lm:singlesegm}, calculate the distance
          $v$ moves when small changes are applied to edge lengths on
          this path and angles between its edges. We show this distance does not exceed the sum of the offsets generated by changes of a single edge or a single angle (see Figures~\ref{fig:anglePathOffset},~\ref{fig:edgePathOffset}). \vspace{-0.25cm}

\end{itemize}

\begin{figure}
	\centering
	\includegraphics[scale=0.72]{figs/angleOffset}
	\caption{The angle between the edge of $\P$ and the edge of $P$ is (locally) less than $\mathcal D\eta$.}
	\label{fig:angleOffset}
\end{figure}

\begin{lemma}
\label{lm:pathAngle}
Let $p_0\ldots p_m$ be a sequence of $m$ edges sharing an endpoint such that $\max
%	\limits_{i=1\ldots m}
|p_{i-1} p_i| \le L$ for some real $L$. Then, for any $j$ from $0$ to
$m-1$, if the portion $p_j\ldots p_m$ of the path is rotated around
$p_j$ by an angle $\theta$ (i.e., such a rotation $\Phi$ is applied to
$p_j\ldots p_m$ so that $\measuredangle p_{j+1} p_j \Phi(p_{j+1}) =
\theta$), then $|p_m \Phi(p_m)| \le 2 mL \sin ( \theta / 2 )$. 
We say that $p_m$ \emph{moves by at most} this value.
\end{lemma}

\begin{proof}
	Apply Lemma~\ref{lm:singlesegm} with $p = p_j$, $q=p_m$, $q'$ being the image of $p_m$ after rotation, $\varepsilon=0$.
\end{proof}

\begin{proof} {\itshape (of Theorem~\ref{precision})} Consider the path $u_1w'_1w'_2\ldots w'_ku$ in the polyhedron $P$ whose vertices correspond to the vertices of $v_1w_1w_2\ldots w_kv$ in $\P$. It contains at most $E$ edges and therefore its total length is at most $EL$.

Consider the vertex $w_i'$ on the path. After applying a rigid transformation to $P$ and $\P$ so that $w'_i$ coincides with $w_i$ and edges $w'_{i-1}w'_i$, $w_{i-1}w_i$ lie on the same ray, then, by the triangle inequality, the angle between $w'_iw'_{i+1}$ and $w_iw_{i+1}$ will not exceed $\mathcal D\gamma$.

Thus the region the edge $w_iw_{i+1}$ of the polyhedron $\P$ can lie inside is a cone: the value of the angle between $w_i w_{i+1}$ and $w'_i w'_{i+1}$ is at most $\mathcal D \gamma$ (see Figure~\ref{fig:angleOffset}).

Let us now make all the edges of the path in $P$ parallel to the corresponding edges of the path in $\P$. We can do that by taking residual paths starting at $w'_1$, then at $w'_2$, $\ldots$ and applying rotations to them, so that the entire path stays connected and its origin is intact (see Figure~\ref{fig:anglePathOffset}). We can also preserve graph structure during the transformation.

By Lemma~\ref{lm:pathAngle}, every time we apply such rotation, the endpoint $u$ of the path moves by at most

\centerline{$E L \cdot 2 \sin ( \mathcal D \gamma / 2 )$.}

Since there are at most $E$ vertices in the path and $E$ rotations are applied, the endpoint $u$ moves by at most
\begin{equation}
	\label{eq:anglePathOffset}
	E^2  L \cdot 2 \sin\ll \frac{\mathcal D \gamma}{2} \rr.
\end{equation}

Now that the directions of all the edges in the path are adjusted, we can make their lengths match the lengths of the corresponding edges in $\P$.

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.46\columnwidth}
		\centering
		\includegraphics[scale=0.82]{figs/anglePathOffset}
		\caption{\ }
		\label{fig:anglePathOffset}
	\end{subfigure}
~
	\centering
	\begin{subfigure}[b]{0.46\columnwidth}
		\centering
		\includegraphics[scale=0.82]{figs/edgePathOffset}
		\caption{\ }
		\label{fig:edgePathOffset}
	\end{subfigure}
\caption{Illustration for the proof of Theorem~\ref{precision}:
	{\itshape (a)}~Rotation by the angle less than $\mathcal D\gamma$
	is applied to the residual path $w'_i\ldots w'_k$. {\itshape (b)}~The edge
	$w'_iw'_{i+1}$ is being lengthened or shortened by at most
	$\mu$. \vspace{-3mm}}
\end{figure}

If the length of a single edge of a path in $P$ is changed by at most $\mu$, and other edges are not changed (as shown in Figure~\ref{fig:edgePathOffset}), then the end of the path also moves by not more than $\mu$. If we stretch any edge by at most $\mu$, the total offset generated by that will be at most
	$E \mu.~\refstepcounter{equation}\hfill(\theequation)\label{eq:edgePathOffset}$

Combining~(\ref{eq:anglePathOffset}) and~(\ref{eq:edgePathOffset}), the total offset does not exceed
	$E^2 L \cdot 2 \sin ( \mathcal D \gamma / 2 ) + E \mu$.
This completes the proof.

\end{proof}