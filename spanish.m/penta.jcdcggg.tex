\documentclass[a4paper,10pt]{article}
\usepackage[margin=1cm,top=0.92cm,bottom=1.02cm]{geometry}
\footskip=0.42cm \columnsep=0.6cm

\usepackage{amsmath,amssymb,amsthm,mathtools}
\usepackage{algorithm,varwidth,tikz}
\usepackage{multicol,wrapfig}
\usepackage[noend]{algpseudocode}
\usepackage{color,xcolor,subcaption}
\usepackage{enumerate,enumitem,hyperref}
\usepackage[utf8x]{inputenc}
\usetikzlibrary{arrows,backgrounds,patterns,matrix,
	shapes,fit,calc,shadows,plotmarks}

\newtheorem{exmpl}{Example}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{stat}[theorem]{Statement}
\newtheorem{prop}[theorem]{Proposition}
\theoremstyle{definition}
\newtheorem{definition}{Definition}
\newtheorem{task}{Problem}
\newtheorem*{remark}{Remark}

\newcounter{secsec}
\setcounter{secsec}{0}

\input{elenasays}

\def\secnm#1{\smallskip\stepcounter{secsec} \noindent{\bf\thesecsec.~#1.}\quad}
\def\secnmd#1{\smallskip\stepcounter{secsec} \noindent{\bf\thesecsec.~#1}\quad}

\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\Align}{\text{{\scshape Align}\,}}

\def\br{\mathbb R} \def\scolon{\rlap{,}\raisebox{0.8ex}{,} }
\def\ll{\left(} \def\rr{\right)}
\def\P{\mathcal P} \def\Q{\mathcal Q}

\begin{document}

\begin{center}
\Large
A complete list of all convex polyhedra made by gluing regular pentagons\footnote{E. A. was supported in part by F.R.S.-FNRS, and by the SNF grant P2TIP2-168563 of the Early PostDoc Mobility program. S. L. is directeur de recherches du F.R.S.-FNRS.}
\end{center}

\begin{center}
Elena Arseneva\footnote{Saint Petersburg State University, Russia},\quad
{\bf Stefan Langerman}\footnote{Expected presenter,
	Universit\'e libre de Bruxelles, Belgium},\quad
and Boris Zolotov\footnote{Saint Petersburg State University, Russia}
\end{center}

\begin{multicols}{2}

\secnm{Abstract} We give a complete description of all convex polyhedra whose
surface can be constructed by folding and gluing (edge-to-edge) regular pentagons.

\secnm{Introduction} Given a collection of 2D polygons, a \emph{gluing} describes a closed
surface by specifying how to glue each polygon edge onto another edge
from the collection.
Alexandrov's uniqueness theorem~\cite{alex} states that any gluing
that is homeomorphic to a sphere and that does not yield 
a total facial angle more than $2\pi$ at any point, is the surface of
a unique convex 3D polyhedron\footnote{Note that the original polygonal pieces
might need to be folded to obtain this 3D surface.}.

Unfortunately, the proof of this theorem is highly non-constructive
and the only known approximation algorithm has (pseudopolynomial)
running time larger than $O(n^{1116})$ (where $n$ is the total number
of edges) \cite{kpd09-approx}, and depends on the
aspect ratio of the polyhedral metric, the Gaussian curvature at its
vertices, and the desired precision of the solution.
There is no known exact algorithm for reconstructing the 3D
polyhedron, and in fact the polyhedron's coordinates might not even have a closed
formula \cite{eppstein-blog}.

Enumerating all possible valid gluings is also not an easy task, as
the number of gluings can be exponential even for a single
polygon~\cite{DDLO02}. However one valid gluing can be found in
polynomial time using dynamic programming~\cite{DO07,lo96-dynprog}.  Complete enumerations of gluings
and the resulting polyhedra are only known for very specific cases
such as the latin cross~\cite{ddlop99} and a single regular polygon~\cite{DO07}.

This paper continues the study, initiated by the first two
authors, for the special case when the polygons to be
glued together are all identical regular $k$-gons. 
For $k>6$,  the only two possibilities are two $k$-gons glued into 
a doubly covered $k$-gon, or one $k$-gon folded in half (if $k$ is
even). When $k=6$, the number of hexagons that can be glued into a
convex surface is unbounded, however there are at most 10 possible graph
structures for such (non-flat) polyhedra, 6 of which have been
identified, and all gluings forming doubly-covered 2D polygons have
been characterized~\cite{kl17-hex}.

Here we study the case $k=5$, i.e.,
gluing regular pentagons edge to edge. This case differs substantially
from the  case of hexagons, since it is not possible to produce a
vertex of Gaussian curvature $0$ by gluing pentagons. Therefore both
the number of possible graph structures and the number of possible
  gluings is constant.
To determine the graph structure of each gluing, we use an
implementation~\cite{sech} of the Bobenko-Izmestiev algorithm to
obtain an approximate polyhedron $P$ for the gluing, and then we use a
computer program to generate a certificate that the approximation $P$ has
the correct graph structure if it is simplicial. 
For non-simplicial polyhedra, we resort to ad-hoc proofs (which appear in
the full version).

\secnm{Gluing regular pentagons together} Let $P$ be a convex 3D polyhedron. The
\emph{Gaussian curvature} at a vertex $v$ of $P$ equals $2\pi - \sum_{j=1}^t{\alpha^v_j}$, where $t$ is the number of faces of $P$ incident to $v$, and  $\alpha^v_j$ 
is the angle of the $j$-th face incident to $v$.  Since $P$ is convex, the
Gaussian curvature at each vertex of $P$ is non-negative. The Gauss-Bonnet theorem (1848) states that the total sum of the Gaussian curvature of all vertices of a 3D polyhedron $P$ equals $4\pi$.

\secnmd{How many pentagons can we glue and which vertices can we obtain?} Let $P$ be a convex polyhedron
 obtained by gluing 
several regular pentagons edge-to-edge. 
 Vertices of $P$ are clearly vertices of the pentagons. The sum of facial angles around a vertex $v$ of $P$ 
equals $3\pi/5$ (the interior angle of a regular pentagon) times 
the number of pentagons glued together at $v$. Since the Gaussian curvature at $v$ is in $(0,2\pi)$, the number of pentagons glued at $v$ can be either one, two, or three. 
This yields   
the Gaussian curvature at $v$ to be respectively $7\pi/5$, $4\pi/5$, or $\pi/5$. 

Note that, as opposed to the case of regular hexagons, it is not possible to produce a vertex of curvature $0$ (which would be a flat point on the surface of $P$) 
by gluing several pentagons. Therefore all the vertices of the pentagons must be vertices of $P$.  

\vspace{-1mm}

\begin{prop}
\label{prop:upperbound}
Suppose $P$ is a convex polyhedron obtained by gluing edge-to-edge $N$ regular pentagons. Then: (a)
 $P$ has $2 + 1.5N$ vertices in total. In particular, $N$ must be even.  
(b)
 $N$ is at most $12$. 
\end{prop}  

\vspace{-4mm}

\paragraph{Enumerating all possible gluings.}
\label{sec:method}
In order to reach our main goal and list all the convex polyhedra that can be obtained by gluing regular pentagons edge-to-edge,
 we used a computer program to list all the non-isomorphic gluings of this type. Our program is a simple modification of 
the one that enumerates the gluings of hexagons~\cite{kl17-hex}.

\input{roadmap_japan}
 
\input{error_quant_japan}

\input{graphics/defs}

\secnm{A complete list of all shapes obtained by gluing pentagons} Figure~\ref{fig:thefulllist} shows all polyhedra that can be obtained by gluing regular pentagons. For those polyhedra that are simplicial, their graph structure is confirmed by applying the method of Section~6, for the others the proof is geometric, and it will apear in the full version of this note.

\secnm{Concluding remarks and future work} The main outcome of this paper is the list of all convex polyhedra
that can be made by gluing several regular pentagons edge to
edge. However the way we obtained this list is interesting in its own
right, and may lead to other results. While this will not always work efficiently for Alexandrov's problem
on arbitrary polyhedral metric (our estimation of $r$ depends
quadratically on the number of vertices of the polyhedron), it should
work well for certain classes of metrics such as metrics with small number of cone points, in particular, for metrics obtained by gluing edge-to-edge regular squares, triangles, or other polygons with fixed angles.
One question that remains open is whether it is possible
to deal with non-simplicial graph structures in a generic way,
i.e., lift the restriction from Theorem~\ref{thm:checkGraphStructure}
that $\P$ is simplicial.

\ \\ [2.4cm]

\columnbreak

\begin{wrapfigure}[1]{l}{1.01\linewidth}
\centering
\begin{subfigure}[b]{0.18\columnwidth}
\begin{center}
	\tikz[scale=0.8]{
	\input{graphics/inscope-1}
	}
\end{center}
	\caption{ }
	\label{fig:1}
\end{subfigure}
~~
\begin{subfigure}[b]{0.28\columnwidth}
	\input{graphics/2}
	\caption{ }
	\label{fig:2}
\end{subfigure}
~~
\begin{subfigure}[b]{0.28\columnwidth}
	\input{graphics/4-1}
	\caption{ }
	\label{fig:4-1}
\end{subfigure}
\\
\begin{subfigure}[b]{0.34\columnwidth}
	\input{graphics/4-2}
	\caption{ }
	\label{fig:4-2}
\end{subfigure}
~~
\begin{subfigure}[b]{0.34\columnwidth}
	\input{graphics/4-3}
	\caption{ }
	\label{fig:4-3}
\end{subfigure}
\\
\begin{subfigure}[b]{0.24\columnwidth}
	\input{graphics/6}
	\caption{ }
	\label{fig:6}
\end{subfigure}
~~
\begin{subfigure}[b]{0.26\columnwidth}
	\input{graphics/8}
	\caption{ }
	\label{fig:8}
\end{subfigure}
~~
\begin{subfigure}[b]{0.28\columnwidth}
\begin{center}
	\tikz[scale=0.53]{
	\input{graphics/inscope-12}
	}
\end{center}
	\caption{ }
	\label{fig:1}
\end{subfigure}
\caption{Polyhedra glued from regular pentagons}
\label{fig:thefulllist}
\end{wrapfigure}

\ \\ [12.8cm]

{\small
	\bibliography{gl}{}
	\bibliographystyle{plain}
}

\end{multicols}
\end{document}