\subsection{Determining the shape from the gluing}

% Радиус alpha → заменить на r
% Plane angle → заменить на facial angle

Consider a gluing $M$ that satisfies Alexandrov's conditions and thus corresponds to unique polyhedron $\P$. Suppose we have a simplicial polyhedron $P$ whose edge lengths and facial angles are close to those of $\P$. In this section we present a procedure that checks whether the graph structures of $P$ and $\P$ coincide.

\def\ball#1{B_r ( #1 )}

We will be using the following notation: $v_1$, $v_2$, $v_3$,~$\ldots$ for the vertices of $\P$; $u_1$, $u_2$, $u_3$,~$\ldots$ for the vertices of $P$; $V$, $E$, $F$ for the number of vertices, edges and faces of $\P$ respectively; $\mathcal D$ for the maximum degree of a vertex of $P$; $L$ for the length of the longest edge of $P$; $\ball{u}$ for the ball in $\br^3$ of radius $r$ centered at the point $u$.

Vertices of $P$ correspond to cone points of metric $M$, and the edges
are the shortest paths between their endpoints. Thus for every edge $u_iu_j$ of $P$ we can calculate the discrepancy between its length and the corresponding length in $M$ (which is the \emph{intended length} of that edge). Let $\mu$ be the maximum of the discrepancy for all edges.

Similarly, for every pair of adjacent edges $u_iu_j$ and $u_iu_k$ of $P$ one can find the discrepancy between the facial angle $u_ju_iu_k$ and the corresponding angle $v_jv_iv_k$ in $M$. We denote the maximum discrepancy between values of angles by $\gamma$.

The basis of our procedure is an observation that each vertex $v_i$ of $\P$ lies within $r$--ball centered at the corresponding vertex $u_i$ of $P$, where
	$r = E^2 \cdot  L \cdot 2 \sin ( \mathcal D \gamma / 2 ) + E \mu$.
We defer its proof to the Section~\ref{section:precis}, see Theorem~\ref{precision}.

Let $u_iu_j$ be an edge of $P$ and let $u_a$, $u_b$ be the two vertices of $P$ opposite to the edge $u_iu_j$. We want to check that there does not exist a plane intersecting all four $r$--balls centered at $u_i$, $u_j$, $u_a$, $u_b$ respectively.

A plane containing a face of the convex polyhedron $P$ divides $\br^3$ into two half-spaces, one of which contains the polyhedron. Assume without loss of generality that the plane passing through $u_a$, $u_i$, $u_j$ is not vertical and that $P$ lies below that plane (otherwise apply a rigid transformation to $P$ so that it becomes true).

Consider three planes $\Pi_1$, $\Pi_2$, $\Pi_3$ tangent to $\ball{u_i}$, $\ball{u_j}$, $\ball{u_a}$ such that:

\begin{itemize}

	\item $\Pi_1$ is below $\ball{u_i}$, $\ball{u_j}$ and above $\ball{u_a}$,
	
	\item $\Pi_2$ is below $\ball{u_i}$ and above $\ball{u_j}$, $\ball{u_a}$,
	
	\item $\Pi_3$ is below $\ball{u_j}$ and above $\ball{u_i}$, $\ball{u_a}$.

\end{itemize}

If $u_b$ lies below $\Pi_1$, $\Pi_2$ and $\Pi_3$ and the distance from
$u_b$ to each of the planes $\Pi_1$, $\Pi_2$ and $\Pi_3$ is greater
than $r$, then the edge $v_iv_j$ must be in $\P$.

This procedure is repeated for every edge $u_iu_j$ of $P$. This implies the following

\begin{theorem}
\label{thm:checkGraphStructure}
	Given a metric $M$ and a simplicial polyhedron $P$ we can check in time  $\mathcal O(E)$ that the convex polyhedron corresponding to metric $M$ has the same graph structure as $P$ without false-positive errors.
\end{theorem}

False negative errors can occur if the precision is not sufficient, and a plane exists that intersects all four $r$--balls centered at the vertices of $P$ even though there is an edge connecting two of the vertices. In such a case precision has to be increased by replacing $P$ with a polyhedron that has smaller discrepancy in edge lengths and values of angles and repeating the procedure.

To obtain polyhedron $P$ in practice one can use the algorithm developed by Kane et al.~\cite{kpd09-approx} or by Bobenko, Izmestiev~\cite{boben}. It outputs a polyhedron $P$ which is an approximation of $\P$. To measure the quality of an approximation the authors introduced the value called \emph{residual curvature}. When running the algorithm, one can set the residual curvature not to exceed a given value $\varkappa$, the lower $\varkappa$ the better approximation will be obtained.